const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    index: './src/index.js',
    print: './src/print.js',
  },
  plugins: [

    new HtmlWebpackPlugin({

      title: 'Output Management',

    }),

  ],
  output: {
    filename: '[name].[contenthash].js',
     path: path.resolve(__dirname, 'dist'),
     clean: true,
   },
 devtool: 'source-map',

};